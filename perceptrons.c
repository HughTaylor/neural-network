#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>

static unsigned int layer0_size;
static const unsigned int layer1_size = 16;  // arbitrary
static const unsigned int layer2_size = 16;  // arbitrary
static const unsigned int layer3_size = 10;  // numbers 0-9
static FILE *f_imgs;
static FILE *f_lbls;
static FILE *f_wgts;
static uint32_t image_cnt;
static uint32_t cols;
static uint32_t rows;
static double *input_data;
static uint8_t *input_labels;
static int weights_num;
static double *neurons;
static double *weights;
static double *biases;

static double
fast_sigmoid(double x)
{
	return x / (1+ abs(x));
}

void
initialise_random_weights(double* weights, int weights_num)
{
	srand(time(NULL));
	for (int i = 0; i < weights_num; i++) {
		weights[i] = (double)rand() / (double)RAND_MAX;
	}
}

double
square(double x)
{
	return x*x;
}

void
open_training_data(void)
{
	/* open the training data files */
	
	if ((f_imgs = fopen("../training_data/train-images-idx3-ubyte", "rb")) == NULL) {
		printf("Error opening training images");
		exit(1);
	}
	if ((f_lbls = fopen("../training_data/train-labels-idx1-ubyte", "rb")) == NULL) {
		printf("Error opening training labels");
		exit(1);
	}
}

uint32_t
read_32be(FILE *inptr)
{
	uint8_t tmp[4];
	uint32_t retval;
	fread(&tmp, sizeof(uint8_t), 4, inptr);
	retval = (uint8_t)tmp[3]<<0 | (uint8_t)tmp[2]<<8 | (uint8_t)tmp[1]<<16 | (uint8_t)tmp[0]<<24;
	return retval;
}

void
read_input_data()
{
	fseek(f_imgs, 0x04, SEEK_SET);

	image_cnt = read_32be(f_imgs);
	rows = read_32be(f_imgs);
	cols = read_32be(f_imgs);

	uint32_t pixelcount = image_cnt * rows * cols;
	input_data = (double *)malloc(pixelcount * sizeof(double));
	
	// copy 8 bytes into tmp, cast to double and divide by 256
	// load into memory at input_data
	uint8_t tmp;
	for (int i = 0; i < pixelcount; i++) { 
		fread(&tmp, sizeof(uint8_t), 1, f_imgs);
		input_data[i] = (double)tmp / (double)255;
	}
	fclose(f_imgs);
}

void
read_input_labels()
{
	uint32_t magic_number = read_32be(f_lbls);
	if (magic_number != 2049) {
		printf("labels file not correctly formatted\n");
		exit(EXIT_FAILURE);
	}
	uint32_t label_cnt = read_32be(f_lbls);
	if (label_cnt != image_cnt) {
		printf("different number of labels to images\n");
		exit(EXIT_FAILURE);
	}
	input_labels = (uint8_t *)malloc(image_cnt * sizeof(uint8_t));
	fread(input_labels, sizeof(uint8_t), image_cnt, f_lbls);
	fclose(f_lbls);
}

double
calculate_cost(int i, double *input_data, uint8_t *input_labels, double *neurons, double *weights)
{
	size_t k;
	size_t j;
	double sum;
	// push the data through the network to set the state of the last neurons.
	// neurons at layer 1 = neurons at layer 0 * weights.
	// initialise layer1
	// for each layer, iterate over each node. for each node, iterate over each node in previous layer (and multiply its value by its weight)
	for (j = 0; j < layer1_size; j++) { 
		double normalised_input;
		for (k = sum = 0; k < layer0_size; k++) { 
			// would be faster to load normalised data into ram instead of recalculating every time
			sum += input_data[k] * weights[k + j];
		}
		neurons[j] = fast_sigmoid(sum);
	}

	// push through layer 2
	for (j = 0; j < layer2_size; j++) {
		for (k = sum = 0; k < layer1_size; k++) {
			sum += neurons[j] * weights[k + j + (layer0_size * layer1_size)];
		}
		neurons[j + layer1_size] = fast_sigmoid(sum);
	}

	// push through layer 3
	for (j = 0; j < layer3_size; j++) {
		for (k = sum = 0; k < layer2_size; k++) {
			sum += neurons[j + layer1_size] * weights[k + j + (layer0_size * layer1_size) + (layer1_size * layer2_size)];
		}
		neurons[j + layer1_size + layer2_size] = fast_sigmoid(sum);
	}
	
	double cost;
	for (j = cost = 0; j < layer3_size; j++) {
		if (input_labels[i] == j) {
			// if the neuron has the correct label
			cost += square(neurons[j + layer1_size + layer2_size] - 1.0);
		} else {
			cost += square(neurons[j + layer1_size + layer2_size] - 0.0);
		}
		
	} 
	return cost;
}

void
allocate_network_memory()
{
	// allocate memory for neural network
	layer0_size = cols * rows;
	weights_num = (layer0_size * layer1_size) + (layer1_size * layer2_size) + (layer2_size * layer3_size);
	neurons = (double*)calloc((layer1_size + layer2_size + layer3_size), sizeof(double));
	weights = (double*)malloc(weights_num * sizeof(double));
	biases = (double*)calloc((layer1_size + layer2_size + layer3_size), sizeof(double));
}

void
read_saved_weights()
{
	// read weights from file, if no file then randomise weights
	if ((f_wgts = fopen("./weights", "rb")) == NULL) {
		printf("No weights file found, randomising weights\n");
		initialise_random_weights(weights, weights_num);
	} else {
		fread(weights, sizeof(double), weights_num, f_wgts); // what if weights file contains different num of weights than weights_num
	};
}

void
free_memory()
{
	free(input_data);
	free(input_labels);
	free(neurons);
	free(weights);
	free(biases);
}

int
main(int argc, char *argv[])
{

	open_training_data();

	read_input_data(); // reads (image_cnt * rows * cols) bytes from f_imgs, divides by 256 and saves as double *input_data
	read_input_labels();

	allocate_network_memory();
	read_saved_weights();
	// calculate cost
//	double cost = 0;
//	for (int i = 0; i < image_cnt; i++)
//	{
//		cost += calculate_cost(i, input_data, input_labels, neurons, weights);
//	};
	

	// debugging
	
//	printf("cost is %lf\n", cost);
	printf("176th pixel is %lf\n", input_data[176]);
	printf("Image count is %i\n", image_cnt);
	printf("cols is %i\n", cols);
	printf("rows is %i\n", rows);
	printf("first neuron is %lf\n", neurons[0]);
	printf("sigmoid is %lf\n", fast_sigmoid(neurons[0]));


	printf("weights num is %i\n", weights_num);
	printf("first weight is %lf\n", weights[0]);

	free_memory();

	return 0;
}
